package PaqValidarNota;
import java.util.Scanner;
public class MainValidarNota {
    
    public static double notaValida(String mensaje,double limInf, double limSup){
        Scanner sc=new Scanner(System.in);
        double nota;
        do{
            System.out.println(mensaje);
            nota=sc.nextDouble();
            if(nota< limInf || nota> limSup){
                System.out.println("Error, nota invalida.");
            }
        }while(nota< limInf || nota> limSup);
        return nota;
    }
    public static String Calificacion(double nota){
        String mensaje;
        mensaje = "";
        if(nota>= 0 && nota < 3){
            mensaje= "MD";
        }else if (nota >= 3 && nota < 5){
            mensaje = "INS";
        }else if (nota >= 5 && nota < 6){
            mensaje = "SUF";
        }else if (nota >= 6 && nota < 7){
            mensaje = "BIE";
        }else if (nota >= 7 && nota < 9){
            mensaje = "NOT";
        }else if (nota >= 9 && nota < 10){
            mensaje = "SOB";
        }else if (nota == 10){
            mensaje = "MH";
        }
        return mensaje;
    }
    
    
    public static void main(String[] args) {
        String mensaje;
        double nota;
        
        nota=notaValida("Introduzca su nota (0-10)", 0, 10);
        mensaje=Calificacion(nota);
        System.out.println("Su nota es: "+mensaje);
        System.out.println("PRUEBA");
    }
    
}

